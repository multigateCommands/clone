#!/usr/bin/perl

use strict;
use warnings;

use 5.10.0;

# standard perl modules
use IO::Select;
use IPC::Open3;
use Symbol qw(gensym);

# globals
my $multidir = $ENV{MULTI_ROOT};
my $dstdir = "${multidir}/commands";

my $is_multicast = $ENV{'MULTI_IS_MULTICAST'};

exit (main(@ARGV) // 0);

sub main {

    my $command = $_[0];

    unless ($command) {
        say "need a command to clone";
        return;
    }

    unless ($command =~ /^\w+$/){
        say "I don't do funny characters in commands";
        return;
    }

    if (-d "$dstdir/$command") {
        say "Command \"$dstdir/$command\" already installed, try pull?";
        return;
    }

    unless (chdir "$dstdir") {
        say "Cannot chdir to \"$dstdir\", giving up";
        return;
    }

    my ($res, $out, $err) = do_command(qw[git clone], "git\@gitlab.com:multigateCommands/$command.git", $command);

    if (defined $res and $res == 0) { # ugh
        if ($is_multicast) {
            if (@$out[0]) {
                say 'clone successful: "', chomp @$out[0], '"';
                say $#$out, ' more lines of output surpressed' if $#$out;
            } else {
                say 'clone succesful but no output?';
            }
        } else {
            say 'clone successful:';
            for (@$out) {
                chomp;
                say;
            }
        }
    } else {
        if ($is_multicast) {
            if (@$err[0]) {
                say 'clone sfailed "', chomp @$err[0], '"';
                say $#$err, ' more lines of errors surpressed' if $#$err;
            } else {
                say 'clone failed without error message?'
            }
        } else {
            # fixme: limit # lines?
            say 'clone failed:';
            for (@$out) {
                chomp;
                say 'out: ', $_;
            }
            for (@$err) {
                chomp;
                say 'err: ', $_;
            }
        }
    }
    
    return;
}

sub do_command {
    my($pid,$cmd_in, $cmd_out, $cmd_err);
    $cmd_err = gensym();

    local $@;
    eval {
        $pid = open3($cmd_in, $cmd_out, $cmd_err, @_);
    };
    return (-1, [], [ $@ ]) if $@;

    my ($res, @out, @err);

    local $SIG{CHLD} = sub {
        $res = $? if waitpid($pid, 0) > 0;
    };

    close($cmd_in);
    
    my $selector = IO::Select->new();
    $selector->add($cmd_out, $cmd_err);

    # this should mostly work but really should use
    # non-blocking filehandles and sysread
    while (my @ready = $selector->can_read) {
        for my $fh (@ready) {
            if (fileno($fh) == fileno($cmd_err)) {
		push @err, scalar <$cmd_err>;
            } else {
                push @out, scalar <$cmd_out>;
            }
            $selector->remove($fh) if eof($fh);
        }
    }

    close($cmd_out);
    close($cmd_err);
    
    return ($res, \@out, \@err);    
}


